<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="colorlib">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Travel</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
		<!--
		CSS
		============================================= -->
		<link rel="stylesheet" href="css/linearicons.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/magnific-popup.css">
		<link rel="stylesheet" href="css/jquery-ui.css">				
		<link rel="stylesheet" href="css/nice-select.css">							
		<link rel="stylesheet" href="css/animate.min.css">
		<link rel="stylesheet" href="css/owl.carousel.css">				
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/custom.css">
	</head>
	<body>	
		<?php
		if( isset($_SESSION['mensagem']) ){
			if( $_SESSION['situacao_cadastro'] ){
				?>
				<div class="mensagem sucesso">
					<?php echo $_SESSION['mensagem']; ?>
				</div>
				<?php
				unset($_SESSION['mensagem']);
			} else {
				?>
				<div class="mensagem erro">
					<?php echo $_SESSION['mensagem']; ?>
				</div>
				<?php
				unset($_SESSION['mensagem']);
			}
		}
		?>
		<header id="header">
			<div class="header-top">
				<div class="container">
		  		<!-- <div class="row align-items-center">
		  			<div class="col-lg-6 col-sm-6 col-6 header-top-left">
		  				<ul>
		  					<li><a href="#">Visit Us</a></li>
		  					<li><a href="#">Buy Tickets</a></li>
		  				</ul>			
		  			</div>
		  			<div class="col-lg-6 col-sm-6 col-6 header-top-right">
						<div class="header-social">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-dribbble"></i></a>
							<a href="#"><i class="fa fa-behance"></i></a>
						</div>
		  			</div>
		  		</div>	 -->		  					
				</div>
			</div>
			<div class="container main-menu">
				<div class="row align-items-center justify-content-between d-flex">
			      <div id="logo">
			        <a href="index.php"><img src="img/logo-2.png" alt="" title="" /></a>
			      </div>
			      <nav id="nav-menu-container">
			        <ul class="nav-menu">
			          <li><a href="index.php">Home</a></li>
			          <!-- <li><a href="about.html">Sobre</a></li> -->
			          <!-- <li><a href="packages.html">Packages</a></li>
			          <li><a href="hotels.html">Hotels</a></li>
			          <li><a href="insurance.html">Insurence</a></li>
			          <li class="menu-has-children"><a href="">Blog</a>
			            <ul>
			              <li><a href="blog-home.html">Blog Home</a></li>
			              <li><a href="blog-single.html">Blog Single</a></li>
			            </ul>
			          </li>	
			          <li class="menu-has-children"><a href="">Pages</a>
			            <ul>
			            	  <li><a href="elements.html">Elements</a></li>
					          <li class="menu-has-children"><a href="">Level 2 </a>
					            <ul>
					              <li><a href="#">Item One</a></li>
					              <li><a href="#">Item Two</a></li>
					            </ul>
					          </li>					                		
			            </ul>
			          </li>	 -->				          					          		          
			          <li><a href="contact.html">Contato</a></li>
			          <li class="li-login"><a href="login.php">Login</a></li>

			        </ul>
			      </nav><!-- #nav-menu-container -->					      		  
				</div>
			</div>
		</header><!-- #header -->
  
		<!-- start banner Area -->
		<section class="relative about-banner">	
			<div class="overlay overlay-bg"></div>
			<div class="container">				
				<div class="row d-flex align-items-center justify-content-center">
					<div class="about-content col-lg-12">
						<h1 class="text-white">
							Cadastro				
						</h1>	
						<p class="text-white link-nav"><a href="index.php">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="contact.html"> Cadastro </a></p>
					</div>	
				</div>
			</div>
		</section>
		<!-- End banner Area -->	

		<section class="contact-page-area section-gap">
			<div class="container">
				<div class="box-login">
					<form class="form-area contact-form text-right" method="post" action="controller/clientes.php">
						<input name="id" type="hidden" value="">
						<input name="nome" placeholder="Nome Completo" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nome Completo'" class="common-input mb-20 form-control" required="required" type="text">

						<input name="data_nascimento" placeholder="Data de Nascimento" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Data de Nascimento'" class="common-input mb-20 form-control" required="required" type="date" >

						<input name="rg" placeholder="Identidade" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Identidade'" class="common-input mb-20 form-control" required="required" type="text" id="rg">

						<input name="cpf" placeholder="CPF" onfocus="this.placeholder = ''" onblur="this.placeholder = 'CPF'" class="common-input mb-20 form-control" required="required" type="text" id="cpf">

						<input name="telefone" placeholder="Telefone" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Telefone'" class="common-input mb-20 form-control" required="required" type="text" id="telefone">
						<input name="celular" placeholder="Celular" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Celular'" class="common-input mb-20 form-control" required="required" type="text" id="celular">
						<input name="email" placeholder="Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'" class="common-input mb-20 form-control" required="required" type="email">

						<input name="senha" placeholder="Senha" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Senha'" class="common-input mb-20 form-control" required="required" type="password">
						<input name="confirmar_senha" placeholder="Confirmar senha" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Confirmar senha'" class="common-input mb-20 form-control" required="required" type="password">

						<div class="alert-msg" style="text-align: left;"></div>
						<button class="genric-btn primary" style="float: right;">Cadastrar</button>
						
					</form>	
				</div>					
			</div>
		</section>		  


		<!-- start footer Area -->		
		<footer class="footer-area section-gap">
			<div class="container">

				<div class="row">
					<div class="col-lg-3  col-md-6 col-sm-6">
						<div class="single-footer-widget">
							<h6>About Agency</h6>
							<p>
								The world has become so fast paced that people don’t want to stand by reading a page of information, they would much rather look at a presentation and understand the message. It has come to a point 
							</p>
						</div>
					</div>
					<div class="col-lg-3 col-md-6 col-sm-6">
						<div class="single-footer-widget">
							<h6>Navigation Links</h6>
							<div class="row">
								<div class="col">
									<ul>
										<li><a href="#">Home</a></li>
										<li><a href="#">Feature</a></li>
										<li><a href="#">Services</a></li>
										<li><a href="#">Portfolio</a></li>
									</ul>
								</div>
								<div class="col">
									<ul>
										<li><a href="#">Team</a></li>
										<li><a href="#">Pricing</a></li>
										<li><a href="#">Blog</a></li>
										<li><a href="#">Contact</a></li>
									</ul>
								</div>										
							</div>							
						</div>
					</div>							
					<div class="col-lg-3  col-md-6 col-sm-6">
						<div class="single-footer-widget">
							<h6>Newsletter</h6>
							<p>
								For business professionals caught between high OEM price and mediocre print and graphic output.									
							</p>								
							<div id="mc_embed_signup">
								<form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="subscription relative">
									<div class="input-group d-flex flex-row">
										<input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
										<button class="btn bb-btn"><span class="lnr lnr-location"></span></button>		
									</div>									
									<div class="mt-10 info"></div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-lg-3  col-md-6 col-sm-6">
						<div class="single-footer-widget mail-chimp">
							<h6 class="mb-20">InstaFeed</h6>
							<ul class="instafeed d-flex flex-wrap">
								<li><img src="img/i1.jpg" alt=""></li>
								<li><img src="img/i2.jpg" alt=""></li>
								<li><img src="img/i3.jpg" alt=""></li>
								<li><img src="img/i4.jpg" alt=""></li>
								<li><img src="img/i5.jpg" alt=""></li>
								<li><img src="img/i6.jpg" alt=""></li>
								<li><img src="img/i7.jpg" alt=""></li>
								<li><img src="img/i8.jpg" alt=""></li>
							</ul>
						</div>
					</div>						
				</div>

				<div class="row footer-bottom d-flex justify-content-between align-items-center">
					<p class="col-lg-8 col-sm-12 footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
					<div class="col-lg-4 col-sm-12 footer-social">
						<a href="#"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-dribbble"></i></a>
						<a href="#"><i class="fa fa-behance"></i></a>
					</div>
				</div>
			</div>
		</footer>
		<!-- End footer Area -->	

		<script src="js/vendor/jquery-2.2.4.min.js"></script>
		<script src="js/popper.min.js"></script>
		<script src="js/vendor/bootstrap.min.js"></script>			
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>		
			<script src="js/jquery-ui.js"></script>					
			<script src="js/easing.min.js"></script>			
		<script src="js/hoverIntent.js"></script>
		<script src="js/superfish.min.js"></script>	
		<script src="js/jquery.ajaxchimp.min.js"></script>
		<script src="js/jquery.magnific-popup.min.js"></script>						
		<script src="js/jquery.nice-select.min.js"></script>					
		<script src="js/owl.carousel.min.js"></script>							
		<script src="js/mail-script.js"></script>	
		<script src="js/main.js"></script>
		<script src="js/jquery.mask.js"></script>	
		<script src="js/custom.js"></script>	
	</body>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#cpf").mask("999.999.999-99");
			$("#rg").mask("999.999.99");
			$("#telefone").mask("(99) 9999-9999");
			$("#celular").mask("(99) 99999-9999");
		});

	</script>
</html>