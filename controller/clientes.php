<?php
	session_start();
	include 'conexao.php';

	$cliente = array(
		'id' => $_POST["id"],
		'nome' => $_POST["nome"],
		'data_nascimento' => $_POST["data_nascimento"],
		'rg' => $_POST["rg"],
		'cpf' => $_POST["cpf"],
		'telefone' => $_POST["telefone"],
		'celular' => $_POST["celular"],
		'email' => (String)$_POST["email"],
		'senha' => (String)$_POST["senha"],
		'confirmar_senha' => $_POST["confirmar_senha"]
	);

	if( empty( $cliente['id']) ){
		try
		{

		$conexao = conn_mysql();

		date_default_timezone_set('America/Sao_Paulo');
		$date = date('Y-m-d H:i');
		echo $date;	
			 			

		$sql = "INSERT INTO clientes (nome, data_nascimento, rg, cpf, telefone, celular, email, senha, created, modifield) VALUES ('".$cliente['nome']."', '".$cliente['data_nascimento']."', '".$cliente['rg']."', '".$cliente['cpf']."', '".$cliente['telefone']."', '".$cliente['celular']."', '".$cliente['email']."', '".$cliente['senha']."', '".$date."', '".$date."');";

		$query = $conexao->prepare($sql);
        $result = $query->execute();
        $operacao = $query->fetchAll();
     	
        $conexao = null;
        $_SESSION['situacao_cadastro'] = true;
        $_SESSION['mensagem'] = "Usuario cadastrado com sucesso!";
        header("Location: ../cadastro-cliente.php");

   		} //try
		catch (PDOException $e)
		{
    	// caso ocorra uma exceção, exibe na tela
		$_SESSION['situacao_cadastro'] = false;
		$_SESSION['mensagem'] = "Ocorreu um erro durante o registo, por favor tente novamente";
		header("Location: ../cadastro-cliente.php");
    	echo "Erro!: " . $e->getMessage() . "<br>";
    	die();
		}

	} else {
		try
		{

		$conexao = conn_mysql();

		date_default_timezone_set('America/Sao_Paulo');
		$date = date('Y-m-d H:i');
		
		$senha_atual = (String)$_POST["senha_atual"];
		$nova_senha = (String)$_POST["nova_senha"];
		//$confirmar_senha = (String)$_POST["confirmar_senha"];

		if( !empty($senha_atual) && !empty($nova_senha)){

			if( strcmp( $senha_atual, $cliente['senha'] ) ){
				$sql = "UPDATE clientes SET nome='".$cliente['nome']."', data_nascimento='".$cliente['data_nascimento']."', rg='".$cliente['rg']."', cpf='".$cliente['cpf']."', telefone='".$cliente['telefone']."', celular='".$cliente['celular']."', email='".$cliente['email']."', senha='".$nova_senha."', modifield='".$date."' WHERE id=".$cliente['id'].";";

				$query = $conexao->prepare($sql);
		        $result = $query->execute();
		        $operacao = $query->fetchAll();

		        $conexao = null;
		        $_SESSION['situacao_cadastro'] = true;
	        	$_SESSION['mensagem'] = "Usuario alterado com sucesso!";
			} else {
				$_SESSION['situacao_cadastro'] = false;
				$_SESSION['mensagem'] = "Senha não confere com a atual";		
			}

		} else {
			$sql = "UPDATE clientes SET nome='".$cliente['nome']."', data_nascimento='".$cliente['data_nascimento']."', rg='".$cliente['rg']."', cpf='".$cliente['cpf']."', telefone='".$cliente['telefone']."', celular='".$cliente['celular']."', email='".$cliente['email']."', modifield='".$date."' WHERE id=".$cliente['id'].";";

			$query = $conexao->prepare($sql);
	        $result = $query->execute();
	        $operacao = $query->fetchAll();

	        $conexao = null;
	        $_SESSION['situacao_cadastro'] = true;
	        $_SESSION['mensagem'] = "Usuario alterado com sucesso!";
	        
		}
		header("Location: ../editar-cliente.php");

   		} //try
		catch (PDOException $e)
		{
    	// caso ocorra uma exceção, exibe na tela
		$_SESSION['situacao_cadastro'] = false;
		$_SESSION['mensagem'] = "Ocorreu um erro durante a alteração, por favor tente novamente";
		header("Location: ../editar-cliente.php");
    	echo "Erro!: " . $e->getMessage() . "<br>";
    	die();
		}


	}

?>